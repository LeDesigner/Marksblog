---
Title: MarkMyLog
Subtitle: December
Date: 2017-12-20
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>


Yessss eindelijk is het December, wellicht de mooiste maand van het jaar! Aankomende week moeten we gaan pitchen en ik ben onwijs benieuwd hoe dat gaat aflopen..

>**4 December**

> - Vandaag hebben we onze pitch volledig voorbereid.
> - We hebben onze concept op 2 A3-vellen getekent...

Gister (maandag) was geen productieve dag. We hadden in de ochtend studiecoaching en we hebben in de middag wellicht ons concept uitgewerkt maar we hebben geen groot werk verricht. Ik hoop dat we dat na de pitch weer kunnen oppakken

>**6 December**

> - Het was zover! pitchen voor de opdrachtgever. Het bleek dat ons concept heel veel op dat van anderen leek en we kwamen erachter dat we de opdracht eigenlijk verkeerd hadden begrepen.
> - We hebben de feedback tot ons genomen en we zijn begonnen aan de Recap.
> - We zaten dus met het issue dat we een nieuw concept wilden bedenken, maar dat iedereen helemaal niet meer inspiritatievol was na de tegenslag.
> - Om deze reden hebben we besloten dat we het even zouden laten varen en dat we er volgende week weer naar gaan kijken.

>**11 December**

> - Vandaag was de dag dat ik met mijn team het ijs moest gaan breken...
> - Er moest namelijk een nieuw concept komen en dat ging niet van het een op het andere moment.
> - Daarom ben ik vandaag ook gaan werken aan de Recap. Deze heb ik bijna helemaal afgekregen en moet ik dus woensdag afmaken.

>**13 December**

> - Na afgelopen maandag heb ik nog te horen gekregen dat 1 van mijn teamleden met een idee is gekomen voor een nieuw concept
> - Deze hebben we vandaag besproken en we zijn gaan kijken naar wat het concept ons allemaal kan brengen
> - Ook heb ik vandaag een workshop gevolgd over Creatieve technieken en daar heb ik ook informatie gekregen over het Design Rationale

Tijdens de workshop van woensdag zijn we erachter gekomen dat we op dit moment alleen maar een middel voor een mogelijk concept hadden bedacht. Het bleek dus dat we nog helemaal geen concept hadden.... dit was een enorme tegenslag en vooral net een weekje voor de vakantie

>**18 December**

> - Vandaag moesten we dan echt met een concept gaan komen. 
> - ik begon de ochtend met studiecoaching en daarna nog de teamcaptainvergadering.
> - In de teamcaptainvergadering bleek dat wij als team niet de enige waren die nog liepen te struggelen met een concept. Dat was opzich fijn om te horen haha
> - We zijn in de middag als team gaan brainstormen over hele extreme concepten. Die zijn makkelijker te verzinnen en we kwamen ook gelijk makkelijker met hele rare ideeen, dat bracht de sfeer wel weer in de groep!
> - Hieruit is uiteindelijk een redelijk extreem concept gekomen maar we gaan proberen dit concept te normaliseren

Vandaag (dinsdag) zouden wij een hoorcollege hebben. Helaas was het niet goed gecommuniceerd maar hij zou zijn verplaatst naar een later moment op de dag... Hier heb ik (en veeeeele andere) niet op gewacht en ik had dus een dagje vrij om te bedenken wat ik met ons extreme concept zou willen doen.

>**20 December**

> - Vandaag was de laatste dag voordat we vakantie hadden. Dit was te merken aan de werkhouding van niet alleen ons groepje, maar van heel de studio.
> - Ik had hier zelf geen problemen mee, het is tenslotte een redelijke drukke periode geweest met veel validaties en deliverables dus iedereen kijk onwijs toe naar de vakantie.
> - Ik heb vanavond het Kerstfeest nog dus mijn dag is nog lang niet afgelopen... x)

"Tussenedit tijden de vakantie": Het feest was onwijs geslaagd!! Super veel mensen uit onze studio waren aanwezig en zo nog veel meer van ons instituut.
Ik ben nu lekker bezig met het bijwerken van wat gevalideerde deliverables en ik ben natuurlijk hard aan het leren voor het Design Theory Tentamen.
