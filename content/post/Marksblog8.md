---
Title: MarkMyLog
Subtitle: April
Date: 2018-04-26
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>

>**9 April**

> - Vandaag was de kickoff van het nieuwe en ook gelijk laatste kwartaal!
> - We hebben na de kickoff onze doelgroep moeten kiezen.
> - Om te bepalen welke doelgroep we namen moesten we een aantal wijken onderzoeken
> - We hebben gekozen voor de wijk Feijenoord!

>**11 April**

> - Vandaag waren we weer met z'n 3en bij elkaar. Dit betekent dat we konden beginnen met het focussen op het project.
> - We zijn begonnen met het maken van een planning. Deze focussed zich vooral op het behalen van de competenties! 
> - Nu we weten wat we moeten doen om de competenties te halen kunnen we ECHT gaan beginnen aan het project...

>**16 April**

> - De week is weer begonnen en het werd tijd om te gaan werken aan het eerste deliverable: het Plan van Aanpak.
> - We hadden al een goed gedeelte van het Plan van Aanpak klaarstaan van vorig kwartaal. Deze konden we voor een deel kopieren maar we moesten deze voor een deel ook opnieuw maken.
> - Hierbij werd de stakeholdersmap door mij bijgewerkt, en maakten Nuray en Ashley de nieuwe planning. We hebben de ontwerpdoelen slechts zo aangepast zodat ze aansloten op de nieuwe doelgroep.

>**18 April**

> - Nadat we afgelopen maandag ons Plan van Aanpak hadden afgemaakt zijn we begonnen met onderzoeken. 
> - Dit begon uiteraard met Deskresearch... Hierbij hebben we ook gekeken naar de competentie 'Onderzoeken' zodat we deze uiteindelijk kunnen behalen.
> - Aan het einde van de dag hebben we voor de competentie 'Samenwerken' een gesprek gevoerd met Mieke (studiecoach). Hier hebben we het gehad over hoe we onze samenwerking vinden gaan en we hebben elkaar feedback mogen geven op ons gedrag en de werkwijze binnen het team.

>**23 april**

> - Vandaag zijn we begonnen met het maken van een Design Rationale... Deze willen wij maken omdat we de competentie 'Empathie' hiermee willen behalen. 
> - Nuray heeft het concept in details beschreven, Ik en Ashley hebben de Design Rationale vormgegeven en de conclusie et. geschreven. 
> - We gaan wilden vandaag al om feedback vragen maar dat kwam er niet van dus we gaan de Design Rationale woensdag afmaken.

>**25 April**

> - Vandaag hebben we feedback op onze Design Rationale gekregen van Robin. Hij heeft ons tips gegeven en een template die we konden aanhouden om de Design Rationale wat overzichtelijker te maken.
> - We zijn begonnen met het invullen van deze template aan de hand van de tekst die we al hadden geschreven. Met deze Design Rationale kunnen we mogelijk 'Empathie' Behalen!

Aankomende vrijdag is het koningsdag en de week daarop is het weer even vakantie! Tijd om uit te rusten en vol goede moede het laatste kwartaal in te gaan!