---
Title: MarkMyLog
Subtitle: November
Date: 2017-11-29
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>


Vanaf November ga ik mijn logboek anders aanpakken... Voorheen werkte ik van onder naar boven. Nu ga ik gewoon van boven naar onder werken zodat het makkelijker is om er in één keer doorheen te lezen

>**14 November**

> - Ik had voor vandaag het verzoek gekregen om naar Den Haag te reizen voor een bezoek aan "PAARD". Het bleek dus dat hier al gelijk de briefing voor kwartaal 2 was!
> - We hebben dus een opdracht vanuit PAARD gekregen om een Clickable Prototype te maken waarmee bezoekers van PAARD vaker terug zouden komen

Zo is het kwartaal dus van start gegaan maar er is nog 1 ding wat mij dwars zit; we hebben nog steeds geen nieuwe teams gevormd...

>**15 November**

> - Vandaag begon de dag dan gelukkig dan wel gelijk met het maken van de nieuwe teams! 
> - We moesten ons verdelen in 4 verschillende categorien waar uiteindelijk teams uit zouden ontstaan met dus groepjes met mensen uit alle categorien.
> - Nadat de groepjes gevormd waren begon ik in mijn groepje met een korte voorstelronde en een teaminventarisatie.
> - We hebben gelijk een mooie planning voor het hele kwartaal gemaakt en we hebben een teamnaam verzonnen; "Toxic Waste" (Hele bijzondere naam maar het kwam voort uit een grapje)
> - In de middag hebben we ook onze doelgroep toegewezen gekregen en hier deze hebben we bestudeerd

Ik ben blij met mijn nieuwe team. Ik heb het gevoel dat iedereen positief is ingesteld en dat we op elkaar kunnen vertrouwen als het gaat om discipline.

>**20 November**

> - Dit is dus de eerste werkweek. Ik ben begonnen met het maken van de deliverables. 
> - Ik moest een debrief maken en hier ben ik uiteindelijk samen met mijn team uitgekomen
> - Nadat ik de debrief af had hebben we met het team een samenwerkingscontract opgesteld. Deze was redelijk snel klaar en daarom hadden we nog even tijd om ons in te lezen over de merkanalyse waar we morgen (woensdag) aan gaan beginnen.

Vandaag (dinsdag) was er ook weer voor het eerst een hoorcollege. Hier begon ik natuurlijk weer heel fanatiek met aantekeningen maken alleen ik denk niet dat ik dat zo lang ga volhouden als de presentaties in zo'n rapvaarttempo gaan... x)

>*22 November**

> - Vandaag heb ik heel de dag aan de merkanalyse gewerkt.
> - Het was in eerste instantie heel onduidelijk wat er precies in de merkanalyse moest staan en ik heb daar ook een tijdje mee zitten tobben.
> - Toen ik het idee had dat ik hem af had ben ik hem gaan valideren. Hier bleek dat ik de merkanalyse vanuit een verkeerd perspectief had benaderd. 
> - Gelukkig heb ik om genoeg feedback gevraagd waardoor ik de merkanalyse alsnog goedgekeurd kon krijgen!

>**27 November**

> - Vanaf deze week moeten we als team gaan kijken naar verschillende deliverables. Zo moet er een User Journey gemaakt worden. Maar ook moeten er 3 Low Fiddelity Prototypes komen en moeten we Ontwerpcriteria maken.
> - Zo ben ik vandaag begonnen met het maken van een User Journey. Deze is uiteindelijk uitgewerkt door ander teamleden.
> - Ook zijn we gaan kijken naar de low fiddelity prototypes. Deze zijn grof geschetst maar de ideeen zijn we aanwezig

>**29 November**

> - Vandaag hebben we onze User Journey gevalideerd en heb ik de ontwerpcriteria gevisualiseerd.
> - Ook hebben we met zijn alle gekeken naar de low fiddelity prototypes. We hebben er 1 gekozen die wij het beste vonden en deze willen we dan ook gaan presenteren volgende week


Dit was mijn Blog voor November. Volgende week is het alweer December (woopwoop) en dan gaan we ons concept pitchen aan de opdrachtgever!