---
title: MarkMyLog
subtitle: September
date: 2017-09-27
tags: ["Fun", "Logboek", "Instafamous", "MOB"]
---


# Welcome to Mark's log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018**  
---

<p>
<p>


Dit was mijn logbloek tot September, In het volgende artikel ga ik verder met de maand Oktober

>**27 September**

> - Vandaag zijn we met zijn 2en begonnen aan het verzamelen van alle deliverables die we tot dan toe hadden. Het bleek dat er nog wat kleine documentjes ontbraken en aangezien we met zijn 2en waren hadden we besloten om deze documentjes individueel te gaan maken. Hier hebben we de hele ochtend voor gebruikt
> - ik heb vanmiddag onze presentatie aan de klas gepresenteerd. het bleek dat de powerpoint totaal niet relevant was en van alle kanten nog niet klopte dus daar hebben we feedback op gehad. Het was maar goed dat we ervoor hadden gekozen om even een proefpresentatie te doen anders hadden we niet goed uit de verf gekomen op de presentatiedag...
> - We hebben samen de feedback verzamelt van klasgenoten en docenten en deze genoteerd en onthouden om zo een betere presentatie neer te zetten


>**25 September**

> - Vandaag waren we weer met zijn 3en... Een teamlid had zijn neus mogelijk gebroken en moest naar het ziekenhuis. Ik moest dus de knoop doorhakken en mijn team aansturen in wat ons te doen stond.
> - Ik heb zelf een spel geanalyseerd aan de hand van een paar standaardvragen die we in iteratie 1 hadden gekregen. Mijn analyse ging over het spel Counter Strike Global Offensive. Dat is een spel wat ik zelf veel heb gespeeld (tpen ik nog gamede) en waar ik veel plezier mee heb gehad.
> - Ook ben ik alvast begonnen aan de presentatie die we volgende week af moeten hebben. Ik heb ons concept laten testen en daar fotos van gemaakt. Die fotos ga ik gebruiken voor het presenteren van ons prototype volgende week


>**20 September**

> - Vandaag waren we maar met zijn 3en. We hebben dan ook de taken opverdeeld in 3..
> - We hebben alle 3 een onderzoek gedaan naar een spel met het nieuwe thema wat we willen gaan gebruiken voor ons protoype. Ik heb zelf voor Minecraft gekozen omdat dat een freeroam spel is en met ons nieuwe thema willen wij ons vooral focussen op games die freeroam gebruiken. Omdat dat heel interessant is voor ons eigen prototype.
> - Ik heb ook weer studiecoaching gevolgd... Dit keer maakte het niet uit hoelaat we stopte omdat ik toch niet meer naar werk moest (de vorge keer was ik een half uur te laat op werk :( )


>**18 September**

> - We hebben vandaag als team onze presentatie gedaan en die ging naar mijn gevoel onwijs goed. We hadden wel te weinig tijd om alles te vertellen dus ik heb wel het gevoel dat de presentatie niet volledig en compleet was maar we hebben er alsnog redelijk wat van gemaakt voor zo'n eerste keer.
> - Ook zijn we vandaag begonnen aan iteratie 2! We moesten onze "darling killen" en ons dus gaan richting op een nieuw prototype... daarvoor moeten we dus opnieuw een planning en teaminventarisatie etc maken. 
> - Ik heb zelf al een planning gemaakt voor het hele team en voor heel iteratie 2 dus ik hoop dat wij ons daar dan aan kunnen houden.

ik heb dit weekend mijn enkelbanden afgescheurd tijdens de voetbal en moet voortaan met krukken naar school :(
<p>

>**15 September**

> - ik heb vandaag al mijn individuele opdrachten in 1 pdf gezet en ingeleverd
> - ook heb ik alle opdrachten voor het team in 1 pdf gezet en op tijd ingeleverd!

we hebben nu alleen morgen nog een werkcollege te gaan...
<p>

>**13 September**

> - vandaag hebben we ons paperprotype voorgelegd aan een aantal personen en hebben wij feedback gehad op ons paperprototype wat wij dan ook gaan gebruiken in onze echte presentatie die we aankomende maandag hebben.
> - we hebben studiecoaching gevolgd en we zijn voorgesteld aan onze peercoach: Wouter van Hattem

Voor zover gaat de communicatie binnen het team redelijk goed, je ziet goed dat ieder zijn eigen taak oppakt alleen hoop ik dat we straks onze taken kunnen verbinden met elkaar en echt als een team kunnen samenwerken
<p>

>**11 September**

> - Ik heb mijn stackedit-blog overgebracht op Gitlab.com :D
> - We zijn als team begonnen aan het maken van ons eerste paperprototype.
> - Ik heb de planning voor het team gemaakt en de takenverdeling en teameigenschappen opgenomen in een word-document en ingeleverd

<p>

>**6 September**

> - Ik heb een visualisatie van mijn onderzoek naar leaderboards en puntentellingen gemaakt
> - Ik heb een concept voor een moodboard over mijn doelgroep gemaakt en deze later op de middag ook uitgewerkt tot een echt moodboard
> - ik ben begonnen met het leren over Stackedit en heb mijn blog vanuit mijn notitieboekje overgenomen op de PC.

<p>

>**5 September**

> - Vandaag hebben we alleen een hoorcollege gehad over het ontwerpproces van designen.
> 
>**Note:** 
>Ik zal de hoorcolleges minder gedetailleerd in de blog zetten omdat die vooral veel theoretische informatie geven en niet echt een activiteit hebben... (Aangezien ik alleen maar stil zit in een grote zaal)

<p>

>**4 September**

> - Brainstormen met mijn team
> - Ik heb een paar beelden getekend en later een paar beelden verwerkt op PowerPoint voor de 
eerste presentatie 
> - We hebben gezamenlijk als team een poster met teameigenschappen erop ontworpen.

Dit was ook mijn eerste officiële schooldag en vanaf het moment dat we klaar waren met het laatste uurtje kon ik niet wachten tot we weer zo'n dag zouden hebben...



> Written with [StackEdit](https://stackedit.io/).