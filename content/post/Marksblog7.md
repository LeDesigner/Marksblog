---
Title: MarkMyLog
Subtitle: Maart
Date: 2018-03-28
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>

De vakantie is weer voorbij en het is tijd om te knallen!

>**5 Maart**

> - We zijn er weer, terug op school.
> - We zijn vandaag verder gegaan met het visualiseren van ons Plan van Aanpak en debrief etc.
> - Deze hebben we uiteindelijk opgehangen aan de wand

>**7 Maart**

> - Vandaag is geen productieve dag geweest. We liepen vast bij de Lifestyle Diary en iedereen heeft een beetje voor zichzelf gewerkt…

>**12 Maart**

> - Vandaag moest ik dan echt verder.
> - Ik heb afgelopen week mijn Lifestyle Diary ingeleverd en deze week gaat hij gevalideerd worden…
> - ondertussen moest ik wel beginnen met het maken van een Lifestyle Diary voor de toekomstige situatie

>**14 Maart**

> - Vandaag is mijn Lifestyle Diary gevalideerd!
> - Ook heb ik druk onderzoek gedaan naar mijn doelgroep.

>**19 Maart**

> - We waren vandaag een dag vrij! Ik heb mijn tijd nuttig besteed door alvast een begin te maken aan mijn concept leerdossier

>**21 Maart**

> - We hebben vandaag hard gebrainstormed over het uiteindelijke concept.
> - We zijn door 3 fases van concepten gekomen en hebben uiteindelijk de knoop doorgehakt; We gaan hologram (weer) maken

Aankomende week wordt een zware week met 2 tentamens en het opleveren van het concept leerdossier. Daarom dat ik dit weekend alvast ben verder gegaan met het maken van STARRT's en het leren van Design Theory.

>**26 Maart**

> - Vandaag zijn we hebben we het low-fid prototype gemaakt. Dit heeft de hele dag geduurd.

>**28 Maart**

> - Gister (Dinsdag) heeft Nuray alvast een beginnetje gemaakt aan het testplan. Deze hebben ik en Ashley uiteindelijk verder uitgewerkt. 
> - Ashley en ik hebben vandaag ook de Recap afgemaakt!


