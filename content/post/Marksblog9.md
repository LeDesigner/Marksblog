---
Title: MarkMyLog
Subtitle: Mei
Date: 2018-05-28
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>

>**2 Mei**

> - Het is tijd om weer te beginnen met school na een geslaagd weekje vakantie! Nu worden het echt de laatste loodjes, tijd om nog even hard aan de bak te gaan!
> - We zijn vandaag verder gegaan met het uitwerken van de Design Rationale, deze wilden we zo snel mogelijk gevalideerd hebben en dat is vandaag nog gelukt...

>**7 Mei**

> - Vandaag zijn we begonnen met het maken van de conceptposter.
> - Helaas zijn we er niet aan toegekomen om feedback te vragen aan een docent en dit moeten dus aankomende woensdag doen...

>**9 Mei**

> - Nu konden we dus wel feedback krijgen op onze conceptposter
> - De feedback was handig en we hebben de poster hier ook gelijk op aangepast

>**14 Mei**

> - Vandaag had ik samen met Ashley besloten om een moodboard te gaan maken. Deze hebben we beide individueel uitgewerkt.
> - Net na de lunch hebben we nog de kans gekregen om hier feedback op te krijgen en we hebben deze vandaag dus nog aanpassen naar een gewenst moodboard.

>**16 Mei**

> - Vandaag ben ik met het hele team gaan werken aan persona's. Deze hebben we gemaakt om samen met de moodboards een goed beeld te krijgen van hoe we ons concept willen gaan uitwerken.
> - Hier hebben we 's middags nog feedback op kunnen krijgen, maar we hebben deze  verder niet meer aangepast...

>**23 Mei**

> - Vandaag werd het dan echt tijd om het concept visueel te maken, aankomende vrijdag is de expo namelijk al...
> - Ik heb besloten om zelf een tutorial filmpje te maken voor het event wat zich zal plaatsvinden in Rotterdam.
> - Ook heb ik nog samen met Nuray en Ashley plattegronden gemaakt waarvan we er uiteindelijk 1 hebben uitgekozen om te creeën alleen dan op de computer.

>**25 Mei**

> - De laatste studiodag van het jaar...
> - Vandaag heb ik mijn video gefinetuned, Ik heb met mijn team gesproken over de expo en we hebben met zijn 3en de expo materialen voorbereid.

Dit was mijn Blog voor jaar 1...
Nu ben ik heel moe en ga ik slapen

