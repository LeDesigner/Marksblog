---
title: MarkMyLog
subtitle: Oktober
date: 2017-10-26
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>


Dit was mijn logboek voor Oktober en ook gelijk voor het eerste kwartaal! Volgende week heb ik mijn eindgesprek en ik hoop dat ik daar goed doorheen kom.

>**25 Oktober**

> - Vandaag was het tijd voor de eindexpo's! We zijn helaas niet in de top 3 gekomen maar we hebben wel zeer ons best gedaan om onze app zo duidelijk mogelijk te pitchen. Dit is naar mijn idee goed gelukt.
> - Aan het einde van de dag ben ik verder gaan werken aan mijn leerdossier en die heb ik uiteindelijk vanavond afgemaakt!

Ik ben dan ook als een malle gaan werken en ik ben daar heel maandagavond nog mee bezig geweest. Dinsdag heeft mijn dag er niet veel anders uit gezien. Ik ben vroeg opgestaan om heel de dag aan mijn leerdossier te werken... 
's Avonds mocht ik weer voor het eerst na 5 weken beginnen met sporten. Ik had namelijk ernstig bandletsel opgelopen tijdens de voetbal en ik moest de eerste 2 weken nog met gips naar school... (zie logboek September) 

>**23 Oktober**

> - Vandaag was het dan weer zover, ik moet weer naar school.
> - Het was wel een onwijs productieve dag. Ik ben nog eens goed gaan kijken naar mijn prototype en heb hem gefinetuned.
> - Ook ben ik vandaag weer veel opgestoken over mijn leerdossier. Ik ben namelijk wezen vergaderen met de feestcommisie en hier kwam na afloop het leerdossier ook ter sprake.
hier heb ik veel goede tips gekregen door 2e jaars en zo ben ik eigenlijk op mijn hoede gezet om maar zo snel mogelijk te beginnen.

>**16 tot 22 Oktober**

> - Ik ben in deze week op vakantie geweest naar New York.. Hele geweldige ervaring maar ik heb geen tijd gehad om aan school te werken.

Vrijdag was het dan zover... De dag voordat we vakantie hadden moest ik nog het tentamen over Design Theory maken. Ik heb mijn spiekbrief goed kunnen gebruiken en nadat we met z'n alle klaar waren hebben we samen wat gegeten om de vakantie met een goed gevoel in te gaan.

>**11 Oktober**

> - Vandaag hebben wij een leerzame workshop van Gerhard (onze studiecoach) gehad over het leerdossier. Hier hebben wij zelf al een STARRT moeten schrijven die uiteindelijk in ons leerdossier zou komen. 
> - Ook heb ik nog een tijdje met leden van de feestcommisie gezeten om wat ontwerpen met elkaar te delen en om een concreet logo te kiezen (uit de logo's die we allemaal individueel hadden gemaakt).
> - Nadat school was afgelopen ben ik naar huis geracet om verder te gaan met het leren van Design Theory

Dinsdag was een dag om te beginnen aan het schrijven van een spiekbrief voor het Design Theory tentamen wat wij aankomende vrijdag hebben.

>**9 Oktober**

> - Vandaag is Iteratie 3 ingegaan! We hebben een nieuwe briefing gekregen genaamd KISS (Keep it simple, stupid).
> - Het is het begin van een korte iteratie, aangezien hij maar 1 week en een dag duurt. Dit omdat we volgende week vakantie hebben. En die week daarop op woensdag hebben wij de al de eindexpo.
> - We zijn vandaag aan de slag gegaan met het verfijnen van ons prototype. Ik ben begonnen aan het ontwerpen van een clickable app.
> - Ook hebben we vandaag feedback ontvangen op onze inlevering van Iteratie 2. We hebben deze feedback ontvangen en opgeslagen omdat deze van pas gaat komen ik volgende kwartalen.
> - We hebben ook nog een nieuwe verbeterde ontwerpproceskaart gemaakt met zijn alle

Donderdag hadden wij ons Nederlands en Engels tentamen. (update: Inmiddels heb ik de uitslag van mijn Nederlands al en die heb ik gelukkig gehaald!)

>**4 Oktober**

> - vandaag was het dan zover, we moesten ons prototype presenteren voor een groep mensen die wij zelf niet kenden. 
> - Het presenteren ging zoals verwacht, we hadden een gestructureerde presentatie en de feedback was van toepassing op onze app (i.p.v. dat we enkel feedback kregen op onze doelgroep, fun, thema etc) wat naar mijn idee een teken was dat we goed hebben nagedacht over de basisconcepten.
> - Nadat we klaar waren met de presentaties ben ik nog even blijven hangen om te vergaderen over mijn personal challenge. 

Dinsdag was voor mij een dag om goed uit te rusten en te werken aan een personal challenge. Ik ben samen met een paar andere 1e en 2e-jaars studenten begonnen aan een feestcommisie waarbij we ook zelf moeten ontwerpen, waar dus tijd in moet worden gestoken.

>**2 Oktober**

> - Vandaag was de laatste studiodag voordat we onze presentatie hadden. We hebben de tijd goed gebruikt om met z'n alle de presentatie goed door te nemen.
> - Ook hebben we de tijd genomen om alle deliverables af te ronden en deze uiteindelijk in 1 pdf bestand te krijgen. 
> - We hebben vooral veel afzonderlijk gewerkt in de middag omdat we allemaal onze eigen documenten klaar moesten hebben en iedereen deed dat liever op zichzelf. Ik werk op zo een moment ook liever op mezelf want ik kan mij beter concentreren op persoonlijke documenten als ik niet al te veel vragen tussendoor krijg.


Ik ga vanaf hier verder waar ik gebleven was in September