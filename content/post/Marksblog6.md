---
Title: MarkMyLog
Subtitle: Februari
Date: 2018-02-15
---



# Welcome to Mark's Log!

#### **Op deze blog houd ik mijn dagen als student van het CMD bij en probeer ik jullie inzicht te geven in waar ik mij op schooldagen mee bezig houd. Ik zal af en toe notities maken om bepaalde activiteiten uit te leggen of te verduidelijken...**

#### **Logboek 2017 - 2018** 
---

<p>
<p>

Het nieuwe kwartaal is weer begonnen, on to a fresh start. We hebben nieuwe teams gevormd en ik ben er erg tevreden mee!!


>**12 Februari**

> - De teams zijn gevormd, Ik zit met Fabian, Nuray en Ashley in het team. Ook Hebben wij Christlain in ons team, maar hier hebben wij nog niks van gehoord.
> - We zijn vandaag ook begonnen aan het maken van het Plan van Aanpak, deze hebben wij voor de helft afgekregen.
> - in de middag heb ik de workshop van competentie 'Inrichten Ontwerpproces' gevolgd. Deze was zeer nuttig omdat ik hierdoor meer inzicht heb gekregen in de compentie

Dinsdag ben ik begonnen als Social Media Designer bij een Makelaarsbedrijf!

>**14 Februari**

> - Vandaag moesten we ons Plan van Aanpak presenteren voor Elske
> - Hiervoor moesten we hem natuurlijk eerst afmaken en we moesten er een poster voor maken.

>**19 Februari**

> - We hebben feedback gekregen op ons Plan van Aanpak en we zijn begonnen met het verwerken van deze feedback. 

>**21 Februari**

> - Vandaag moesten we onze Plan van Aanpak opnieuw valideren. Dit keer is het wel stukken beter gegaan. 
> - Met een paar aanpassen is ons Plan van Aanpak namelijk wel gevalideerd

En nu is het tijd voor een weekje vakantie!!



